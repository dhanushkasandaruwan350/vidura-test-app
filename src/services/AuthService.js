import DATA_SERVICE from "./DataService";
import _API from "../util/Api";
import deviceStorage from "../util/deviceStorage";

const REQUEST_TYPE = {
    GET: 'GET',
    POST: 'POST'
};
const HEADERS = {};
const AuthService = {
    async _sign_in(data) {
        let execute = await DATA_SERVICE.execute(_API._ENDPOINTS._AUTH._SIGN_IN, data, REQUEST_TYPE.POST, HEADERS);
        if (execute.status) {
            const token = {token: execute.result.data.accessToken};
            await deviceStorage.saveKey('token', JSON.stringify(token));
            return execute;
        } else {
            return execute;
        }
    },
    async _sign_up(data) {
        let execute = await DATA_SERVICE.execute(_API._ENDPOINTS._AUTH._SIGN_UP, data, REQUEST_TYPE.POST, HEADERS);
        return execute.status;
    },
};

export default AuthService;
