import axios from 'axios';


const DATA_SERVICE = {

    async execute(endpoint, data, type, headers) {
        if (type === 'POST') {
            return await this.postData(endpoint, data, headers);
        } else if (type === 'GET') {
            return await this.getData(endpoint, data, headers);
        }
    },
    async getData(endpoint, data, headers) {
        return await axios.get(endpoint, {params: data})
            .then((response) => {
                return {status: true, data: response.data, message: 'ok'};
            }).catch((error) => {
                return {status: true, data: 'empty', message: error}
            });
    },
    async postData(endpoint, data, headers) {
        if (data.data !== undefined) data = data.data;
        return await axios.post(endpoint, data)
            .then((response) => {
                return {status: true, result: response, message: 'ok'};
            })
            .catch((error) => {
                console.log(error);
                return {status: false, result: '', message: error};
            });
    }
};
export default DATA_SERVICE;
