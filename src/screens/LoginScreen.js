import React from 'react';
import {SafeAreaView, StyleSheet, TouchableOpacity, View, Image, ScrollView} from 'react-native';
import TextStyle from "../components/Text";
import InputStyle from "../components/Input";
import {Fontisto, MaterialIcons} from "@expo/vector-icons";
import deviceStorage from "../util/deviceStorage";
import AuthService from "../services/AuthService";
import AlertStyle from "../components/Alert";


export default class LoginScreen extends React.Component {
    constructor() {
        super();
        this.state = {
            email: '',
            pass: '',
            image_uri: 'default',
            showAlert: false,
            alertTitle: '',
            alertMessage: '',
            showWaiting: false
        }
    }

    async componentDidMount() {
        const img = await deviceStorage.getItem('img');
        console.log(img);
        if (img !== null && img !== undefined && img.length > 5) {
            this.setState({image_uri: img});
        }
    }

    render() {
        const {showAlert} = this.state;
        return (<SafeAreaView style={styles.container}>
            <ScrollView>
                <View style={styles.top}>
                    <View style={{width: '50%'}}>
                        <TextStyle text="myzone" color="#964ff0" margin={35} padding={1} size={32} weight="700"
                                   align="center"/>
                        <TextStyle text='Please Enter Account Details You Wish To Access' color="#9c9c9f" margin={0}
                                   padding={1} size={10}
                                   weight="500" align="center"/>
                    </View>
                    <View style={{width: '50%', marginTop: 55}}>
                        {this.state.image_uri === 'default' ? <Image source={require('../../assets/anonymous.png')}
                                                                     style={{
                                                                         width: "100%",
                                                                         height: 150,
                                                                         marginBottom: 30,
                                                                         borderRadius: 25
                                                                     }}
                                                                     resizeMode="center"
                        /> : <Image source={{uri: this.state.image_uri}}
                                    style={{width: "100%", height: 200, borderRadius: 25}}
                                    resizeMode="center"
                        />}
                    </View>
                </View>
                <View style={styles.center}>
                    <InputStyle placeholder="Your Account Email" onChange={(text) => this.setState({email: text})}/>
                    <InputStyle placeholder="Password" pass={true} onChange={(text) => this.setState({pass: text})}/>
                </View>
                <View style={styles.center}>
                    {this.state.showWaiting === false ? <TouchableOpacity style={styles.touchButton}
                                                                          onPress={() => this.signIn()}
                                                                          delayPressIn={0}>
                        <MaterialIcons name="lock-open" size={64} color="#ffffff"/>
                    </TouchableOpacity> : <TouchableOpacity style={styles.touchButton}
                                                            delayPressIn={0}>
                        <Fontisto name="spinner" color="#ffffff" size={45} style={{marginRight: 10}}/>
                    </TouchableOpacity>}
                    <TouchableOpacity style={styles.footer} onPress={() => this.forgotPassword()}>
                        <Fontisto name="confused" color="#F75454" size={20} style={{marginRight: 10}}/>
                        <TextStyle text="I FORGOT MY ACCOUNT" color="#F75454" weight="700"/>
                    </TouchableOpacity>
                </View>
                <View style={styles.separator}/>
                <TouchableOpacity style={styles.signup} onPress={() => this.navigate_signup()}>
                    <Fontisto name="room" color="#964ff0" size={45} style={{marginRight: 10}}/>
                    <TextStyle text="Join With My Zone Community" color="#964ff0" weight="700" size={10}/>
                </TouchableOpacity>
                <AlertStyle onCancelPressed={() => this.setState({showAlert: false})}
                            onConfirmPressed={() => this.setState({showAlert: false})}
                            show={showAlert}
                            title={this.state.alertTitle}
                            message={this.state.alertMessage}
                            cancel="Cancel"
                            confirm="Okay"/>
            </ScrollView>
        </SafeAreaView>);
    }

    forgotPassword() {
        this.setState({
            alertTitle: 'Oops!',
            alertMessage: 'Sorry Not Implemented!',
            showAlert: true
        });
    }

    navigate_signup() {
        this.props.navigation.navigate('register');
    }

    async signIn() {
        this.setState({showWaiting: true});
        const data = {
            email: this.state.email,
            pass: this.state.pass
        };
        const res = await AuthService._sign_in(data);
        if (res.status === false) {
            this.setState({
                alertTitle: 'Account Not Found!',
                alertMessage: 'Please double check your account details and try again.',
                showAlert: true
            });
            this.setState({showWaiting: false});
            return;
        }
        await deviceStorage.saveKey('user', JSON.stringify(res.result.data));
        this.setState({showWaiting: false});
        this.props.navigation.navigate('profile');
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1e1e1e'
    },
    top: {
        flexDirection: 'row',
        marginTop: 10
    },
    center: {
        marginTop: 40,
    },
    separator: {
        marginVertical: 8,
        borderBottomColor: '#737373',
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
    touchButton: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8,
        marginTop: 10,
        borderRadius: 100
    },
    footer: {
        flexDirection: 'row',
        marginTop: 25,
        marginRight: 10,
        marginBottom: 10,
        justifyContent: 'flex-end',
    },
    signup: {
        marginVertical: 12,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
