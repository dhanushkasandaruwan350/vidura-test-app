import React, {Component} from 'react';
import {SafeAreaView, StyleSheet, TouchableOpacity, View, StatusBar} from 'react-native';
import {Fontisto, MaterialIcons} from "@expo/vector-icons";
import TextStyle from '../components/Text';
import * as Facebook from 'expo-facebook';

export default class TouchScreen extends Component {
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <TextStyle text="myzone" color="#964ff0" margin={35} padding={1} size={32} weight="700"
                           align="center"/>
                <TouchableOpacity style={styles.touch}>
                    <View bgColor="#1e1e1e">
                        <View bgColor="#5196f405" style={styles.circle}>
                            <View bgColor="#5196f410" style={styles.circle}>
                                <View bgColor="#5196f430" style={styles.circle}>
                                    <TouchableOpacity style={styles.touchButton}
                                                      onLongPress={() => this.navigate_signin()}
                                                      delayPressIn={0}>
                                        <MaterialIcons name="fingerprint" size={64} color="#ffffff"/>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
                <TextStyle text='Welcome To My Zone Community' color="white" margin={32} padding={1} size={15}
                           weight="700" align="center"/>
                <TextStyle text='Press And Hold To Verify' color="#9c9c9f" margin={0} padding={1} size={10}
                           weight="500" align="center"/>
                <TextStyle text='Your Identity' color="#9c9c9f" margin={0} padding={1} size={10} weight="500"
                           align="center"/>
                <TextStyle text='' color="#9c9c9f" margin={10} padding={1} size={10} weight="500" align="center"/>

                <TouchableOpacity style={styles.fb} onPress={() => this.logIn()}>
                    <Fontisto name="facebook" color="#964ff0" size={16} style={{marginRight: 10}}/>
                    <TextStyle text="Access With Facebook" color="#964ff0" weight="700"/>
                </TouchableOpacity>
                <StatusBar barStyle="light-content"/>
            </SafeAreaView>
        );
    }

    async logIn() {
        try {
            await Facebook.initializeAsync({
                appId: '327277665352892',
            });
            const {
                type,
                token
            } = await Facebook.logInWithReadPermissionsAsync({
                permissions: ['public_profile'],
            });
            if (type === 'success') {
                const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);
                Alert.alert('Logged in!', `Hi ${(await response.json()).name}!`);
            } else {
                // type === 'cancel'
            }
        } catch ({message}) {
            alert(`Facebook Login Error: ${message}`);
        }
    }


    navigate_signin() {
        this.props.navigation.navigate('login');
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1e1e1e'
    },
    touch: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    circle: {
        backgroundColor: '#5196F430',
        padding: 32,
        borderRadius: 999
    },
    touchButton: {
        backgroundColor: '#5196f4',
        padding: 8,
        borderRadius: 100
    },
    fb: {
        marginTop: 16,
        marginBottom: 16,
        padding: 16,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    }
});
