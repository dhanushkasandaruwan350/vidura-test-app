import React, {Component} from 'react';
import {SafeAreaView, StyleSheet, TouchableOpacity, View, Image, ScrollView} from 'react-native';
import TextStyle from "../components/Text";
import InputStyle from "../components/Input";
import {Fontisto, MaterialIcons} from "@expo/vector-icons";
import AuthService from "../services/AuthService";
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';
import * as FileSystem from "expo-file-system";
import deviceStorage from "../util/deviceStorage";
import AlertStyle from "../components/Alert";

export default class RegistrationScreen extends Component {
    constructor() {
        super();
        this.state = {
            first: '',
            last: '',
            email: '',
            pass: '',
            image: '',
            image_uri: '',
            showAlert: false,
            alertTitle: '',
            alertMessage: '',
            showWaiting: false
        }
    }

    render() {
        const {showAlert} = this.state;
        return (<SafeAreaView style={styles.container}>
            <ScrollView>
                <View style={styles.top}>
                    <View style={{width: '50%'}}>
                        <TextStyle text="myzone" color="#964ff0" margin={35} padding={1} size={32} weight="700"
                                   align="center"/>
                        <TextStyle text="Are You Ready To Join With Us?" color="white" margin={0}
                                   padding={1} size={10}
                                   weight="700" align="center"/>
                        <TextStyle text="Let's Create Your My Zone Account." color="#9c9c9f" margin={0}
                                   padding={1} size={10}
                                   weight="500" align="center"/>
                    </View>
                    <View style={{width: '50%', marginTop: 55}}>
                        <TouchableOpacity onPress={() => this.chooseImage()}>
                            {this.state.image_uri.length < 1 ? <Image source={require('../../assets/man.png')}
                                                                      style={{
                                                                          width: "100%",
                                                                          height: 200,
                                                                          borderRadius: 25
                                                                      }}
                                                                      resizeMode="center"
                            /> : <Image source={{uri: this.state.image_uri}}
                                        style={{width: "100%", height: 200, borderRadius: 25}}
                                        resizeMode="center"
                            />}
                            <TextStyle text="Click To Upload Image" color="#9c9c9f" margin={2}
                                       padding={1} size={10}
                                       weight="500" align="center"/>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.center}>
                    <InputStyle placeholder="First Name" onChange={(text) => this.setState({first: text})}/>
                    <InputStyle placeholder="Last Name" onChange={(text) => this.setState({last: text})}/>
                    <InputStyle placeholder="Email" onChange={(text) => this.setState({email: text})}/>
                    <InputStyle placeholder="Password" pass={true} onChange={(text) => this.setState({pass: text})}/>
                </View>
                {this.state.showWaiting === false ?
                    <TouchableOpacity style={styles.footer} onPress={() => this.create_account()}>
                        <TextStyle text="CREATE MY ACCOUNT" color="#964ff0" weight="700" size={20}/>
                    </TouchableOpacity> : <TouchableOpacity style={styles.touchButton}
                                                            delayPressIn={0}>
                        <Fontisto name="spinner" color="#ffffff" size={45} style={{marginRight: 10}}/>
                    </TouchableOpacity>}
                <View style={styles.separator}/>
                <TouchableOpacity style={styles.signup} onPress={() => this.navigate_signin()}>
                    <Fontisto name="persons" color="white" size={45} style={{marginRight: 10}}/>
                    <TextStyle text="I Already Have An Account" color="#9c9c9f" weight="700" size={10}/>
                </TouchableOpacity>
                <AlertStyle onCancelPressed={() => this.setState({showAlert: false})}
                            onConfirmPressed={() => this.setState({showAlert: false})}
                            show={showAlert}
                            title={this.state.alertTitle}
                            message={this.state.alertMessage}
                            cancel="Cancel"
                            confirm="Okay"/>
            </ScrollView>
        </SafeAreaView>);
    }

    navigate_signin() {
        this.props.navigation.navigate('login');
    }

    create_account() {
        if (this.state.first === '' || this.state.first.length < 3) {
            this.setState({
                alertTitle: '',
                alertMessage: 'Your first name is not acceptable.\nMinimum 3 characters required!',
                showAlert: true
            });
            return;
        } else if (this.state.last === '' || this.state.last.length < 3) {
            this.setState({
                alertTitle: '',
                alertMessage: 'Minimum 3 characters required for the last name.',
                showAlert: true
            });
            return;
        } else if (this.state.email === '' || this.state.email.length < 4) {
            this.setState({
                alertTitle: '',
                alertMessage: 'Please enter valid email!',
                showAlert: true
            });
            return;
        } else if (this.state.pass === '' || this.state.pass.length < 4) {
            this.setState({
                alertTitle: '',
                alertMessage: 'Your password is too week!',
                showAlert: true
            });
            return;
        } else if (this.state.image.length === '' || this.state.image.length < 4) {
            alert('Please select an image!');
            return;
        }
        this.proceed_with_account_creation();
    }

    async proceed_with_account_creation() {
        this.setState({showWaiting: true});
        const data = {
            fname: this.state.first,
            lname: this.state.last,
            email: this.state.email,
            password: this.state.pass,
            image: this.state.image
        };
        const res = await AuthService._sign_up(data);
        this.setState({showWaiting: false});
        if (res) {
            this.props.navigation.navigate('login');
        } else {
            this.setState({
                alertTitle: 'Hmm!',
                alertMessage: 'Something is not right.\nPlease try again later!',
                showAlert: true
            });
        }
    }

    async chooseImage() {
        await Permissions.askAsync(Permissions.MEDIA_LIBRARY);
        const image = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true
        });
        if (!image.cancelled) {
            this.setState({image_uri: image.uri});
            this.setState({image: await FileSystem.readAsStringAsync(image.uri, {encoding: 'base64'})});
            await deviceStorage.saveKey('img', image.uri);
        }
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1e1e1e'
    },
    top: {

        flexDirection: 'row',
        marginTop: 10
    },
    center: {},
    separator: {
        marginVertical: 8,
        borderBottomColor: '#737373',
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
    touchButton: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8,
        marginTop: 10,
        borderRadius: 100
    },
    footer: {
        flexDirection: 'row',
        marginTop: 25,
        marginRight: 10,
        marginBottom: 10,
        justifyContent: 'center',
    },
    signup: {
        marginVertical: 12,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
