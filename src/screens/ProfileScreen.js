import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, View, Image} from 'react-native';
import {Fontisto, MaterialIcons} from "@expo/vector-icons";
import TextStyle from '../components/Text';
import deviceStorage from "../util/deviceStorage";

export default class TouchScreen extends Component {
    constructor() {
        super();
        this.state = {
            user: '',
            showImage: 'https://bootdey.com/img/Content/avatar/avatar6.png'
        }
    }

    async componentDidMount() {
        const user = JSON.parse(await deviceStorage.getItem('user'));
        const img = await deviceStorage.getItem('img');
        this.setState({user: user});
        if (user.image === 'NO IMAGE' && img !== 'NO IMAGE') {
            this.setState({showImage: img});
        } else {
            this.setState({showImage: user.image});
        }

    }

    render() {
        const {showImage} = this.state;
        return (
            <View style={styles.container}>
                <View style={styles.header}></View>
                <Image style={styles.avatar} source={{uri: showImage}}/>
                <View style={styles.title}>
                    <TextStyle text="myzone" color="#964ff0" margin={35} padding={1} size={32} weight="700"
                               align="center"/>
                    <View style={{alignItems: 'flex-end', marginTop: -70}}>
                        <TextStyle text={'Welcome Back ' + this.state.user.lname + '!'} color="white" margin={32}
                                   padding={1} size={15}
                                   weight="700" align="center"/>
                    </View>
                    <View style={styles.separator}/>
                </View>
                <TouchableOpacity style={styles.info}>
                    <Fontisto name="male" color="#F75454" size={25} style={{marginRight: 10}}/>
                    <TextStyle text={this.state.user.fname + ' ' + this.state.user.lname} color="#9c9c9f" weight="700"
                               size={15}/>
                </TouchableOpacity>
                <TouchableOpacity style={styles.info}>
                    <Fontisto name="email" color="#F75454" size={25} style={{marginRight: 10}}/>
                    <TextStyle text={this.state.user.email} color="#9c9c9f" weight="700" size={15}/>
                </TouchableOpacity>
                <View style={{alignItems: 'flex-start', marginTop: 1}}>
                    <TextStyle text={'$9,435.17'} color="#9c9c9f" margin={32}
                               padding={1} size={30}
                               weight="700" align="center"/>
                    <View style={{alignItems: 'center', marginTop: -35, marginLeft: 53}}>
                        <TextStyle text={'Earned From MyZone'} color="#9c9c9f" margin={0}
                                   padding={0} size={10}
                                   weight="500" align="center"/>
                    </View>
                </View>
                <View style={{flexDirection: 'row', alignItems: 'flex-start', marginTop: 35, marginHorizontal: 10}}>
                    <Fontisto name="angularjs" color="#9c9c9f" size={50} style={{marginRight: 14, marginLeft: 10}}/>
                    <Fontisto name="java" color="#9c9c9f" size={50} style={{marginRight: 14}}/>
                    <Fontisto name="react" color="#9c9c9f" size={50} style={{marginRight: 14}}/>
                    <Fontisto name="mysql" color="#9c9c9f" size={50} style={{marginRight: 14}}/>
                    <Fontisto name="oracle" color="#9c9c9f" size={50} style={{marginRight: 14}}/>
                    <Fontisto name="gitlab" color="#9c9c9f" size={50} style={{marginRight: 14}}/>
                </View>
                <TouchableOpacity style={styles.touch}>
                    <View bgColor="#1e1e1e">
                        <View bgColor="#5196f430" style={styles.circle}>
                            <TouchableOpacity style={styles.touchButton}
                                              onPress={() => this.logOut()}
                                              delayPressIn={0}>
                                <MaterialIcons name="logout" size={34} color="red"/>
                            </TouchableOpacity>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }

    logOut() {
        deviceStorage.removeItem('user');
        this.props.navigation.navigate('Touch');
    }
}

const styles = StyleSheet.create({
    title: {
        alignItems: 'flex-start',
    },
    header: {
        backgroundColor: '#1e1e1e',
        height: 200,
    },
    avatar: {
        width: 130,
        height: 130,
        borderRadius: 63,
        borderWidth: 4,
        borderColor: "white",
        marginBottom: 10,
        alignSelf: 'center',
        position: 'absolute',
        marginTop: 130
    },
    container: {
        flex: 1,
        backgroundColor: 'rgba(7,7,7,0.77)',
    },
    touch: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        marginRight: 10,
        marginBottom: 10
    },
    circle: {
        backgroundColor: 'rgba(255,61,137,0.19)',
        padding: 12,
        borderRadius: 999
    },
    touchButton: {
        backgroundColor: 'white',
        padding: 8,
        borderRadius: 100
    },
    info: {
        flexDirection: 'row',
        marginVertical: 12,
        marginHorizontal: 10,
        alignItems: 'flex-start',
        justifyContent: 'flex-start'
    }
});
