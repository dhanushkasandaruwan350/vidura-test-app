const _HTTP_PREFIX = 'http://143.110.177.128:8080/api/';

const _API = {
    _ENDPOINTS: {
        _AUTH: {
            _SIGN_IN: _HTTP_PREFIX + 'auth/sign-in',
            _SIGN_UP: _HTTP_PREFIX + 'auth/sign-up',
        }
    }
};
export default _API;
