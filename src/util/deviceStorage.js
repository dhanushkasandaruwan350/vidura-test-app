import {AsyncStorage} from 'react-native';

const deviceStorage = {
    async saveKey(key, value) {
        try {
            await AsyncStorage.setItem(key, value);
        } catch (error) {
            console.log('AsyncStorage Error: ' + error.message);
        }
    },
    async getItem(key) {
        try {
            return await AsyncStorage.getItem(key);
        } catch (exception) {
            return false;
        }
    },
    async removeItem(key) {
        try {
            await AsyncStorage.removeItem(key);
            return true;
        } catch (exception) {
            return false;
        }
    }

};

export default deviceStorage;
