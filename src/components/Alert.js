import React, {Component} from 'react';
import AwesomeAlert from 'react-native-awesome-alerts';
export default class AlertStyle extends Component {
    render() {
        return (
            <AwesomeAlert
                show={this.props.show}
                showProgress={false}
                title={this.props.title}
                message={this.props.message}
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                showCancelButton={true}
                showConfirmButton={true}
                cancelText={this.props.cancel}
                confirmText={this.props.confirm}
                confirmButtonColor="#DD6B55"
                onCancelPressed={() => {
                    this.props.onCancelPressed();
                }}
                onConfirmPressed={() => {
                    this.props.onConfirmPressed();
                }}
            />
        );
    }

}
