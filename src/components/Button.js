import React, {Component} from 'react';
import {Button} from 'react-native';

export default class ButtonStyle extends Component {
    render() {
        return (
            <Button title={this.props.text} color="#841584"/>
        );
    }

}
