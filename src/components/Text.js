import React, {Component} from 'react';
import {Text, View, Image, TextInput, SafeAreaView, StyleSheet} from 'react-native';

export default class TextStyle extends Component {
    render() {
        return (
            <Text style={{
                color: this.props.color ?? 'white',
                margin: this.props.margin ?? 0,
                padding: this.props.padding ?? 0,
                fontSize: this.props.size ?? 13,
                fontWeight: this.props.weight ?? '400',
                textAlign: this.props.align ?? 'left'
            }}>{this.props.text}</Text>
        );
    }

}
