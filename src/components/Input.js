import React, {Component} from 'react';
import {TextInput} from 'react-native';

export default class InputStyle extends Component {
    render() {
        return (
            <TextInput placeholder={this.props.placeholder} placeholderTextColor="#9c9c9f" style={{
                color: 'white',
                borderColor:'#1e1e1e',
                textAlign: 'left', fontWeight: '600', fontSize: 15,
                marginHorizontal: 20,
                borderBottomColor: '#9c9c9f', borderWidth: 0.5,
                height: 40, marginVertical: 10,
                borderBottomRightRadius:7
            }} secureTextEntry={this.props.pass ?? false}
                       onChangeText={(text) => this.props.onChange(text)}/>
        );
    }

}
