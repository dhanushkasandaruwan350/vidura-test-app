import React from 'react';
import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";
import TouchScreen from "./src/screens/TouchScreen";
import LoginScreen from "./src/screens/LoginScreen";
import RegistrationScreen from "./src/screens/Registration";
import ProfileScreen from "./src/screens/ProfileScreen";
import * as Font from 'expo-font';

export default class App extends React.Component {
    async componentDidMount() {
        await Font.loadAsync({
            // 'fontisto-brands': require('@expo/vector-i'),
            // 'fontisto-editors': require('./assets/fonts/fontisto/fontisto-editors.ttf'),
        });
    }

    render() {
        const AppStack = createStackNavigator();
        return (
            <NavigationContainer>
                <AppStack.Navigator headerMode="none">
                    <AppStack.Screen name="Touch" component={TouchScreen}/>
                    <AppStack.Screen name="login" component={LoginScreen}/>
                    <AppStack.Screen name="register" component={RegistrationScreen}/>
                    <AppStack.Screen name="profile" component={ProfileScreen}/>
                </AppStack.Navigator>
            </NavigationContainer>
        );
    }

}
